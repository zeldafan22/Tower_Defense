using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;
using System;

public class MenuManager : MonoBehaviour
{

    public TMP_Text play_Text;
    public GameObject MainMenuUI;
    public GameObject LevelSelectUI;
    public GameObject SettingsMenuUI;
    public Slider effectSlider;
    public Slider musicSlider;

    private void Awake()
    {
        
        if (SceneManager.GetActiveScene().buildIndex != 9)
        {
            var temp = (int)(GameConfigurationManager.Instance.playtime + Time.unscaledTime);
            play_Text.text = "Playtime: " + TimeSpan.FromSeconds(temp).Hours + "h " + TimeSpan.FromSeconds(temp).Minutes + "min.";
        }
    }

    public void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex != 9)
        {
            var temp = (int)(GameConfigurationManager.Instance.playtime + Time.unscaledTime);
            play_Text.text = "Playtime: " + TimeSpan.FromSeconds(temp).Hours + "h " + TimeSpan.FromSeconds(temp).Minutes + "min.";
        }
        effectSlider.value = GameConfigurationManager.Instance.effectVolume;
        musicSlider.value = GameConfigurationManager.Instance.musicVolume;

    }

    public void StartNew()
    {
        MainMenuUI.SetActive(false);
        LevelSelectUI.SetActive(true);
        if (GameConfigurationManager.Instance.unlockedLevel <=9)
        {
            var aux = GameConfigurationManager.Instance.unlockedLevel;

            for (int i = 1; i < aux; i++)
            {
                var aux2 = LevelSelectUI.GetComponentsInChildren<Button>();
                aux2[i].GetComponentsInChildren<Image>()[1].enabled = false;
            }
        }
    }
    public void GoBack()
    {
        MainMenuUI.SetActive(true);
        LevelSelectUI.SetActive(false);
        SettingsMenuUI.SetActive(false);
        GameConfigurationManager.Instance.qualityLevel = QualitySettings.GetQualityLevel();
        GameConfigurationManager.Instance.SaveLevel();
    }
    public void LevelSelect(int id)
    {
        if(GameConfigurationManager.Instance.unlockedLevel >= id)
        {
            SceneManager.LoadScene(id);
        }
        else
        {
            var aux = LevelSelectUI.GetComponentsInChildren<Button>();
            
            aux[id-1].GetComponentInChildren<TMP_Text>().color = Color.red;

        }

    }
    public void Exit()
    {
        GameConfigurationManager.Instance.playtime += Time.unscaledTime;
        GameConfigurationManager.Instance.SaveLevel();

#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit(); // original code to quit Unity player
#endif
    }

    public void Settings()
    {
        MainMenuUI.SetActive(false);
        SettingsMenuUI.SetActive(true);
    }

    public void WipeData()
    {

        GameConfigurationManager.Instance.unlockedLevel = 1;
        GameConfigurationManager.Instance.effectVolume = 0.5f;
        GameConfigurationManager.Instance.musicVolume = 0.5f;
        GameConfigurationManager.Instance.playtime = 0f;
        play_Text.text = "Playtime: 0h 0min.";
        GameConfigurationManager.Instance.qualityLevel = 1;
        GameConfigurationManager.Instance.SaveLevel();
    }
}
