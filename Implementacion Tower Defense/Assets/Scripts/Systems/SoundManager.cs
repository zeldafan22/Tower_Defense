using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public AudioSource backgroundMusic;
    public bool randomized;
     
    // Start is called before the first frame update
    void Start()
    {
        this.backgroundMusic.volume = GameConfigurationManager.Instance.musicVolume;
        if (randomized)
        {
            if (SceneManager.GetActiveScene().buildIndex > 4)
            {
                this.backgroundMusic.clip = GameConfigurationManager.Instance.music_clips[SceneManager.GetActiveScene().buildIndex - 5];
            }
            else
            {
                this.backgroundMusic.clip = GameConfigurationManager.Instance.music_clips[SceneManager.GetActiveScene().buildIndex - 1];
            }
            
            this.backgroundMusic.Play();
        }
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateEffectVolume(Slider slider)
    {
        GameConfigurationManager.Instance.effectVolume = slider.value;
    }
    public void UpdateMusicVolume(Slider slider)
    {
        GameConfigurationManager.Instance.musicVolume = slider.value;
        this.backgroundMusic.volume = GameConfigurationManager.Instance.musicVolume;
    }
}
