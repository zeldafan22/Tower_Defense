using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.AI;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.UI;

public class GameConfigurationManager : MonoBehaviour
{

    public static GameConfigurationManager Instance;
    public string PlayerName;
    public float effectVolume;
    public float musicVolume;
    public int unlockedLevel;
    public float playtime;
    public int qualityLevel;
    public AudioSource efectos;
    public List<AudioClip> music_clips = new List<AudioClip>();

    [System.Serializable]
    class SaveData
    {
        public int unlockedLevel;
        public float playtime = 0;
        public int qualityLevel;
        public float effectVolume;
        public float musicVolume;
    }
    public void SaveLevel()
    {
        SaveData data = new SaveData();
        data.unlockedLevel = GameConfigurationManager.Instance.unlockedLevel;
        data.playtime = GameConfigurationManager.Instance.playtime;
        data.effectVolume = GameConfigurationManager.Instance.effectVolume;
        data.musicVolume = GameConfigurationManager.Instance.musicVolume;
        data.qualityLevel = GameConfigurationManager.Instance.qualityLevel;

        string json = JsonUtility.ToJson(data);

        File.WriteAllText(Application.persistentDataPath + "/savefile.json", json);
        Debug.Log(GameConfigurationManager.Instance.unlockedLevel);
    }

    public void LoadData()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            SaveData data = JsonUtility.FromJson<SaveData>(json);

            GameConfigurationManager.Instance.unlockedLevel = data.unlockedLevel;
            GameConfigurationManager.Instance.effectVolume = data.effectVolume;
            GameConfigurationManager.Instance.musicVolume = data.musicVolume;
            GameConfigurationManager.Instance.playtime = data.playtime;
            GameConfigurationManager.Instance.qualityLevel = data.qualityLevel;

        }
        else
        {
            GameConfigurationManager.Instance.unlockedLevel = 1;
            GameConfigurationManager.Instance.effectVolume = 0.5f;
            GameConfigurationManager.Instance.musicVolume = 0.5f;
            GameConfigurationManager.Instance.playtime = 0f;
            GameConfigurationManager.Instance.qualityLevel = 1;
        }
        QualitySettings.SetQualityLevel(GameConfigurationManager.Instance.qualityLevel);
        GameConfigurationManager.Instance.efectos = FindObjectOfType<AudioSource>();
    }

    public void changeQuality(int level)
    {
        GameConfigurationManager.Instance.qualityLevel = level;
        QualitySettings.SetQualityLevel(level);
    }

    public void restoreSettings()
    {
        GameConfigurationManager.Instance.qualityLevel = 1;
        GameConfigurationManager.Instance.effectVolume = 1f;
        GameConfigurationManager.Instance.musicVolume = 0.5f;
        QualitySettings.SetQualityLevel(1);
        FindObjectOfType<SoundManager>().backgroundMusic.volume = 0.5f;

    }

    private void Awake()
    {
        // start of new code
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        // end of new code

        Instance = this;
        LoadData();
        DontDestroyOnLoad(gameObject);
    }
   
    // Start is called before the first frame update
   

}
