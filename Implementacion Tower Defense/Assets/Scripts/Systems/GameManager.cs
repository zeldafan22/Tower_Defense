using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int unlockedLevel = 0;
    public int money;
    public TMP_Text moneyText;
    public int lives;
    public TMP_Text livesText;
    public int wave;
    public int lastWave;
    public TMP_Text waveText;
    public bool isGameOver;
    public GameObject waveButton;
    public GameObject waveCanvas;
    public GameObject pauseCanvas;
    public GameObject defeatScreen;
    public GameObject victoryScreen;

    public float levelCurve;
    public int upgradeLevel;

    public Slider effectSlider;
    public Slider musicSlider;

    public Transform startingPosition;
    public Transform endingPosition;
    public NavMeshHit endGameNavMeshHit;

    public SpawnManager spawnManager;


    private void Awake()
    {

        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        Instance.unlockedLevel = 0;
        Instance.wave = 0;
        Instance.isGameOver = false;
        Instance.spawnManager = spawnManager;
        Instance.moneyText = moneyText;
        Instance.livesText = livesText;
        Instance.waveText = waveText;
        Instance.startingPosition = startingPosition;
        Instance.endingPosition = endingPosition;
        Instance.lastWave = Instance.spawnManager.waveList.Count - 1;
        Instance.waveButton = waveButton;
        Instance.waveCanvas = waveCanvas;
        Instance.pauseCanvas = pauseCanvas;
        Instance.defeatScreen = defeatScreen;
        Instance.victoryScreen = victoryScreen;
        Instance.effectSlider = effectSlider;
        Instance.effectSlider.value = GameConfigurationManager.Instance.effectVolume;
        Instance.musicSlider = musicSlider;
        Instance.musicSlider.value = GameConfigurationManager.Instance.musicVolume;
        Instance.levelCurve = 1f;
        Instance.upgradeLevel = 0;
        TowerManager.isMenuOpen = false;
        PauseMenu.IsPaused = false;
        switch (SceneManager.GetActiveScene().name)
        {
            //Ajustar para que lo general este antes del switch y lo de cada nivel (dinero y vidas) dentro
            case "Tutorial":
                Debug.Log(SceneManager.GetActiveScene().buildIndex);
                Instance.money = 50;
                Instance.lives = 10;
                break;
            case "Level 1":
                Instance.money = 50;
                Instance.lives = 10;
                break;
            case "Level 2":
                Instance.money = 100;
                Instance.lives = 10;
                break;
            case "Level 3":
                Instance.money = 100;
                Instance.lives = 20;
                Instance.upgradeLevel = 1;
                break;
            case "Level 4":
                Instance.money = 125;
                Instance.lives = 20;
                Instance.upgradeLevel = 1;
                break;
            case "Level 5":
                Instance.money = 150;
                Instance.lives = 20;
                Instance.upgradeLevel = 2;
                break;
            case "Level 6":
                Instance.money = 100;
                Instance.lives = 25;
                Instance.upgradeLevel = 2;
                break;
            case "Level 7":
                Instance.money = 250;
                Instance.lives = 50;
                Instance.upgradeLevel = 2;
                break;
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        Instance.UpdateWave();
        Instance.UpdateMoney();
        Instance.UpdateLives();
        Instance.effectSlider.value = GameConfigurationManager.Instance.effectVolume;
        Instance.musicSlider.value = GameConfigurationManager.Instance.musicVolume;
        GameConfigurationManager.Instance.efectos = FindObjectOfType<AudioSource>();
    }

    // Update is called once per frame
    private void Update()
    {

        if (Instance.lives <= 0 && Instance.isGameOver == false)
        {
            Debug.Log("You just lost the game");
            Time.timeScale = 0f;
            GameConfigurationManager.Instance.efectos.clip = GameConfigurationManager.Instance.music_clips[5];
            GameConfigurationManager.Instance.efectos.loop = false;
            GameConfigurationManager.Instance.efectos.Play();
            Instance.isGameOver = true;
            Instance.waveCanvas.SetActive(false);
            Instance.pauseCanvas.SetActive(false);
            Instance.defeatScreen.GetComponentsInChildren<TMP_Text>()[1].text = 
            "Aliens invaded your base, you survived until wave "+ (Instance.wave+1) +"/"+ (Instance.lastWave+1) +
            ". \n \n Press exit to go back to the main menu or retry to start this stage again";
            Instance.defeatScreen.SetActive(true);
        }   
    }

    public void UpdateMoney()
    {
        Instance.moneyText.text = "" + Instance.money;
    }

    public void UpdateLives() 
    {
        Instance.livesText.text = "" + Instance.lives;
    }

    public void UpdateWave()
    {
        Instance.waveText.text = "Wave: " + (Instance.wave+1) + "/" + (Instance.lastWave+1);
    }

    public void reloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void loadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void changequality(int level)
    {
        GameConfigurationManager.Instance.changeQuality(level);
    }
    public void restoresetting()
    {
        GameConfigurationManager.Instance.restoreSettings();
        GameManager.Instance.musicSlider.value = GameConfigurationManager.Instance.musicVolume;
    }
    
    public void closeGame()
    {
        GameConfigurationManager.Instance.playtime += Time.unscaledTime;
        GameConfigurationManager.Instance.SaveLevel();
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit(); // original code to quit Unity player
#endif
    }
}