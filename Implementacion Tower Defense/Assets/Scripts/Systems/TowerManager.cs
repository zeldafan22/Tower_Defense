using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using DG.Tweening;

public class TowerManager : MonoBehaviour
{
    [SerializeField]
    private List<Tower> placeableTowers = new List<Tower>();
    [SerializeField] private Tower towerCheck;
    public Tower tempTower;
    public GameObject towerMenuUI;
    public GameObject towerUpgradeMenuUI;
    public int towerIndex;
    public bool validPaths;
    public static bool isMenuOpen = false;
    public static Ray towerPosition;
    public static bool checkingPath = false;
    public Vector3 clickedPosition;

    public RaycastHit temp_ray;

    private void Start()
    {
        validPaths = true;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0) && isMenuOpen == false && PauseMenu.IsPaused == false && GameManager.Instance.isGameOver==false)
        {
            HandleNewClick();
        }
        

    }
    private void HandleNewClick()
    {

        //He hecho click en el terreno? (If)
        towerPosition = Camera.main.ScreenPointToRay(Input.mousePosition);
        bool objectHit = Physics.Raycast(towerPosition, out RaycastHit hitInfo);
        if(objectHit && hitInfo.collider.tag == "Floor")
        {
            clickedPosition = new Vector3(hitInfo.transform.position.x, (float)(hitInfo.point.y + 1.5), hitInfo.transform.position.z); ;
            StartCoroutine(CheckPath(hitInfo));   

        }else if(objectHit && hitInfo.collider.tag == "Tower")
        {
            Time.timeScale = 0f;
            isMenuOpen = true;
            
            tempTower = hitInfo.collider.gameObject.GetComponent<Tower>();

            var aux = towerUpgradeMenuUI.GetComponentsInChildren<TMP_Text>();
            if (tempTower.targetType == targetType.First)
            {
                towerUpgradeMenuUI.GetComponentInChildren<TMP_Dropdown>().value = 0;
            }
            else if (tempTower.targetType == targetType.Last)
            {
                towerUpgradeMenuUI.GetComponentInChildren<TMP_Dropdown>().value = 1;
            }
            else if (tempTower.targetType == targetType.Strongest)
            {
                towerUpgradeMenuUI.GetComponentInChildren<TMP_Dropdown>().value = 2;
            }
            else
            {
                towerUpgradeMenuUI.GetComponentInChildren<TMP_Dropdown>().value = 3;
            }


                aux[0].text = tempTower.towerName;
            aux[1].text = "<b>Tower Range:</b> " + tempTower.range;
            aux[2].text = "<b>Tower Damage:</b> " + tempTower.damage;
            aux[3].text = "<b>Attack Speed:</b> " + tempTower.attackSpeed;
            aux[4].text = "<b>Target Type:</b> " + tempTower.targetHit.ToString();
            var auxString = (tempTower.currentLevel.ToString()).Substring(tempTower.currentLevel.ToString().Length-1);


            if(aux[6].text=="Option A")
            {
                aux[7].text = "<b>Current Level:</b> " + auxString;
                aux[6].text = "" + tempTower.targetType;
                if(tempTower.isBossKiller == true)
                {
                    aux[8].text = "<b>Boss Killer:</b> Yes";
                }
                else
                {
                    aux[8].text = "<b>Boss Killer:</b> No";
                }
            }
            else
            {
                aux[6].text = "<b>Current Level:</b> " + auxString;
                aux[5].text = "" + tempTower.targetType;
                if (tempTower.isBossKiller == true)
                {
                    aux[7].text = "<b>Boss Killer:</b> Yes";
                }
                else
                {
                    aux[7].text = "<b>Boss Killer:</b> No";
                }
            }
            
            
            if(GameManager.Instance.upgradeLevel == 0)
            {
                towerUpgradeMenuUI.GetComponentsInChildren<Button>()[0].GetComponentInChildren<TMP_Text>().text = "Locked";
            }
            else
            {
                if (GameManager.Instance.upgradeLevel == 1)
                {
                    if (tempTower.currentLevel == (upgradeLevel)System.Enum.Parse(typeof(upgradeLevel), "nivel1"))
                    {
                        towerUpgradeMenuUI.GetComponentsInChildren<Button>()[0].GetComponentInChildren<TMP_Text>().text = "Locked";
                    }
                    else if (tempTower.currentLevel == (upgradeLevel)System.Enum.Parse(typeof(upgradeLevel), "nivel2"))
                    {
                        towerUpgradeMenuUI.GetComponentsInChildren<Button>()[0].GetComponentInChildren<TMP_Text>().text = "Max Level";
                    }
                    else
                    {
                        towerUpgradeMenuUI.GetComponentsInChildren<Button>()[0].GetComponentInChildren<TMP_Text>().text = "Upgrade \n(" + tempTower.upgradePrice + ")";
                    }
                }else
                {
                    Debug.Log("Dentro nivel 2");
                    if (tempTower.currentLevel != (upgradeLevel)System.Enum.Parse(typeof(upgradeLevel), "nivel2"))
                    {
                        towerUpgradeMenuUI.GetComponentsInChildren<Button>()[0].GetComponentInChildren<TMP_Text>().text = "Upgrade \n(" + tempTower.upgradePrice + ")";
                    }
                    else
                    {
                        towerUpgradeMenuUI.GetComponentsInChildren<Button>()[0].GetComponentInChildren<TMP_Text>().text = "Max Level";
                    }
                }
                
            }
            

            towerUpgradeMenuUI.GetComponentsInChildren<Button>()[1].GetComponentInChildren<TMP_Text>().text = "Sell \n(" + tempTower.sellPrice + ")";

            towerUpgradeMenuUI.SetActive(true);

            Debug.Log("Abriendo menu torre");
            
        }
        else
        {
            if (objectHit && hitInfo.collider.tag == "Forbidden")
            {
                if (hitInfo.Equals(temp_ray))
                {
                    DOTween.KillAll();
                }
                
                GameObject temp = hitInfo.collider.gameObject;
                var renderer = temp.GetComponentInChildren<MeshRenderer>();
                renderer.material.color = Color.red;
                renderer.material.DOColor(Color.white, 0.5f);
                temp_ray = hitInfo;
                

            }
            
        }
    }
 
    private IEnumerator CheckPath(RaycastHit hitInfo)
    {
        checkingPath = true;
        var purchasingTower = Instantiate(towerCheck);
        purchasingTower.transform.position = new Vector3(hitInfo.transform.position.x, (float)(hitInfo.point.y + 1.5), hitInfo.transform.position.z);
        yield return null;
        yield return null;
        bool hasPath = true;
        NavMeshPath pathStartEnd = new NavMeshPath();
        NavMesh.CalculatePath(GameManager.Instance.startingPosition.position, GameManager.Instance.endingPosition.position, NavMesh.AllAreas, pathStartEnd);
        Debug.Log(pathStartEnd.status);
        if(pathStartEnd.status == NavMeshPathStatus.PathComplete)
        {
            for (int i = 0; i < GameManager.Instance.spawnManager.enemyList.Count; i++)
            {
                NavMeshPath pathEnemy = new NavMeshPath();
                NavMesh.SamplePosition(GameManager.Instance.endingPosition.position, out GameManager.Instance.endGameNavMeshHit, 5, NavMesh.AllAreas);
                hasPath = GameManager.Instance.spawnManager.enemyList[i].myNavMeshAgent.CalculatePath(GameManager.Instance.endGameNavMeshHit.position, pathEnemy);

                if (pathEnemy.status != NavMeshPathStatus.PathComplete)
                {
                    hasPath = false;
                    break;
                }
            }
        }
        else
        {
            hasPath = false;
        }
        Destroy(purchasingTower.gameObject);
        purchasingTower = null;
        for (int i = 0; i < GameManager.Instance.spawnManager.enemyList.Count; i++)
        {
            GameManager.Instance.spawnManager.enemyList[i].myNavMeshAgent.SetDestination(GameManager.Instance.endGameNavMeshHit.position);
        }
        if (hasPath)
        {
            isMenuOpen = true;
            towerIndex = 0;
            DisplayInformation(0);
            towerMenuUI.SetActive(true);
            Time.timeScale = 0f;
        }
        else
        {
            if (hitInfo.Equals(temp_ray))
            {
                DOTween.KillAll();
            }
            hitInfo.collider.gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.red;
            hitInfo.collider.gameObject.GetComponentInChildren<MeshRenderer>().material.DOColor(Color.white, 0.5f);
            temp_ray = hitInfo;

        }

        checkingPath = false;

    }

    private IEnumerator DelayMenu(float time)
    {
        yield return new WaitForSecondsRealtime(time);
            isMenuOpen = false;
    }

    #region purchaseMenu
    

    public void ExitPurchaseMenu()
    {
        towerMenuUI.SetActive(false);
        Time.timeScale = 1f;
        StartCoroutine(DelayMenu(.25f));
    }

    public void PlaceTower()
    {
            Tower placingTower = Instantiate(placeableTowers[towerIndex]);
            placingTower.transform.position = clickedPosition;
            ExitPurchaseMenu();
    }

    public void DisplayInformation(int index)
    {
        var aux = towerMenuUI.GetComponentsInChildren<TMP_Text>();
        aux[0].text = placeableTowers[index].towerName;
        aux[1].text = placeableTowers[index].towerDescription;
        aux[2].text = "<b>Purchase Price:</b> " + placeableTowers[index].purchasePrice;
        aux[3].text = "<b>Tower Range:</b> " + placeableTowers[index].range;
        aux[4].text = "<b>Tower Damage:</b> " + placeableTowers[index].damage;
        aux[5].text = "<b>Attack Speed:</b> " + placeableTowers[index].attackSpeed;
        aux[6].text = "<b>Boss Killer:</b> " + placeableTowers[index].isBossKiller;
        
        towerIndex = index;
    }

    public void PurchaseTower()
    {

            if ((GameManager.Instance.money - placeableTowers[towerIndex].purchasePrice) >= 0)
            {
                GameManager.Instance.money -= (int)placeableTowers[towerIndex].purchasePrice;
                GameManager.Instance.UpdateMoney();
                PlaceTower();
            }
            else
            {
            DOTween.KillAll();
            Debug.Log("Sin dinero");
            
            var button = GameObject.Find("/TowerManager/TowerMenu/Panel/Comprar").GetComponent<Button>();
            var texto_dinero = GameObject.Find("/UI/PauseMenu/moneyText").GetComponent<TMP_Text>();
            texto_dinero.color = Color.red;
            button.image.color =  Color.red;
            texto_dinero.DOColor(Color.white, 2).SetUpdate(true);
            button.image.DOColor(Color.white, 1).SetUpdate(true);
        }

    }


    #endregion

    #region TowerUpgradeMenu
    public void changeTarget(TMP_Dropdown dropdown)
    {
        tempTower.targetType = (targetType)System.Enum.Parse(typeof(targetType), dropdown.value.ToString());
        tempTower.changedTarget = true;
    }

    public void upgradeTower()
    {
        if(GameManager.Instance.upgradeLevel == 1)
        {
            if (tempTower.currentLevel == upgradeLevel.nivel0)
            {
                if (GameManager.Instance.money - tempTower.upgradePrice >= 0)
                {
                    var auxPos = tempTower.transform.position;
                    Tower auxTower = tempTower.towerUpgrade;
                    Destroy(tempTower.gameObject);
                    auxTower = Instantiate(auxTower);
                    auxTower.gameObject.transform.position = auxPos;
                    GameManager.Instance.money -= tempTower.upgradePrice;
                    GameManager.Instance.UpdateMoney();
                    closeTowerUpgradeMenu();
                }else
                {
                    Debug.Log("Dinero insuficiente");
                    //Meter shader para cambiar bot�n a transici�n rojo -> original
                    DOTween.KillAll();
                    var button = GameObject.Find("/UI/TowerUpgradeMenu/Panel/Upgrade").GetComponent<Button>();
                    var texto_dinero = GameObject.Find("/UI/PauseMenu/moneyText").GetComponent<TMP_Text>();
                    texto_dinero.color = Color.red;
                    button.image.color = Color.red;
                    texto_dinero.DOColor(Color.white, 2).SetUpdate(true);
                    button.image.DOColor(Color.white, 1).SetUpdate(true);
                }
                
            }
            else
            {
                Debug.Log("nivel m�ximo");
                //Meter shader para cambiar bot�n a transici�n rojo -> original
                DOTween.KillAll();
                var button = GameObject.Find("/UI/TowerUpgradeMenu/Panel/Upgrade").GetComponent<Button>();
                button.image.color = Color.red;
                button.image.DOColor(Color.white, 1).SetUpdate(true);
            }
        }
        else if (GameManager.Instance.upgradeLevel == 2)
        {
            if (tempTower.currentLevel != upgradeLevel.nivel2)
            {
                if (GameManager.Instance.money - tempTower.upgradePrice >= 0)
                {
                    var auxPos = tempTower.transform.position;
                    Tower auxTower = tempTower.towerUpgrade;
                    Destroy(tempTower.gameObject);
                    auxTower = Instantiate(auxTower);
                    auxTower.gameObject.transform.position = auxPos;
                    GameManager.Instance.money -= tempTower.upgradePrice;
                    GameManager.Instance.UpdateMoney();
                    closeTowerUpgradeMenu();
                }
                else
                {
                    Debug.Log("Dinero insuficiente");
                    //Meter shader para cambiar bot�n a transici�n rojo -> original
                    DOTween.KillAll();
                    var button = GameObject.Find("/UI/TowerUpgradeMenu/Panel/Upgrade").GetComponent<Button>();
                    var texto_dinero = GameObject.Find("/UI/PauseMenu/moneyText").GetComponent<TMP_Text>();
                    texto_dinero.color = Color.red;
                    button.image.color = Color.red;
                    texto_dinero.DOColor(Color.white, 5).SetUpdate(true);
                    button.image.DOColor(Color.white, 1).SetUpdate(true);
                }

            }
            else
            {
                Debug.Log("nivel m�ximo");
                //Meter shader para cambiar bot�n a transici�n rojo -> original
                DOTween.KillAll();
                var button = GameObject.Find("/UI/TowerUpgradeMenu/Panel/Upgrade").GetComponent<Button>();
                button.image.color = Color.red;
                button.image.DOColor(Color.white, 1).SetUpdate(true);
            }
        }
        else
        {
            Debug.Log("nivel m�ximo");
            //Meter shader para cambiar bot�n a transici�n rojo -> original
            DOTween.KillAll();
            var button = GameObject.Find("/UI/TowerUpgradeMenu/Panel/Upgrade").GetComponent<Button>();
            button.image.color = Color.red;
            button.image.DOColor(Color.white, 1).SetUpdate(true);
        }

    }

    public void sellTower()
    {
        GameManager.Instance.money += tempTower.sellPrice;
        GameManager.Instance.UpdateMoney();
        Destroy(tempTower.gameObject);
        closeTowerUpgradeMenu();
    }

    public void closeTowerUpgradeMenu()
    {
        tempTower = null;
        Time.timeScale = 1f;
        isMenuOpen = false;
        towerUpgradeMenuUI.SetActive(false);
    }
    #endregion
}
