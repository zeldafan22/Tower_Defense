using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpawnManager : MonoBehaviour
{
    //Variables
    public Enemy myPrefab;
    public bool isWaveOver;
    public bool isStageFinished;
    public int spawnedEnemies;
    public List<Enemy> enemyList = new List<Enemy>();
    public Image background;

    [System.Serializable]
    public class WaveObject
    {
        public float timePerCreation = 1;
        public List<Enemy> enemyList = new List<Enemy>();
    }
    
    public List<WaveObject> waveList = new List<WaveObject>();


    // Funciones
    void Start()
    {
        spawnedEnemies = 0;
        isWaveOver = true;
        background = GameManager.Instance.waveCanvas.GetComponentInChildren<Image>();

    }

    public void StartNextWave()
    {
        if (isWaveOver == true && TowerManager.isMenuOpen == false)
        {
            isWaveOver = false;
            spawnedEnemies = 0;
            StartCoroutine(spawnWave());
            GameManager.Instance.waveButton.SetActive(false);
            background.gameObject.SetActive(false);
        }
    }

    public IEnumerator spawnWave()
    {

        if(isWaveOver == true)
        {
            yield break;
        }
        for (int i = 0; i < waveList[GameManager.Instance.wave].enemyList.Count; i++)
        {
            Enemy myEnemy = Instantiate(waveList[GameManager.Instance.wave].enemyList[i], GameManager.Instance.startingPosition.position, GameManager.Instance.startingPosition.rotation);
            enemyList.Add(myEnemy);
            spawnedEnemies++;
            yield return new WaitForSeconds(waveList[GameManager.Instance.wave].timePerCreation);
        }
    }

    // Update is called once per frame
    void Update()
    {
            
        if(waveList[GameManager.Instance.wave].enemyList.Count == spawnedEnemies && enemyList.Count == 0 && isWaveOver == false)
        {
            if(GameManager.Instance.wave < GameManager.Instance.lastWave)
            {
                GameManager.Instance.wave++;
                if(SceneManager.GetActiveScene().buildIndex == 7 || SceneManager.GetActiveScene().buildIndex == 8)
                {
                    if (GameManager.Instance.wave % 5 == 0)
                    {
                        GameManager.Instance.levelCurve += 0.5f;
                    }
                }else if (SceneManager.GetActiveScene().buildIndex == 6)
                {
                    GameManager.Instance.levelCurve += 0.15f;
                }
                isWaveOver = true;
                
                GameManager.Instance.UpdateWave();
                GameManager.Instance.waveButton.SetActive(true);
                background.gameObject.SetActive(true);


            }
            else if (GameManager.Instance.wave == GameManager.Instance.lastWave && isStageFinished == false && GameManager.Instance.lives > 0){
                var nivel_actual = SceneManager.GetActiveScene().buildIndex;
                if(nivel_actual == 8)
                {
                    SceneManager.LoadScene(9);
                    
                }
                else
                {
                    Debug.Log("Has ganado");
                    isStageFinished = true;
                    Time.timeScale = 0f;
                    GameConfigurationManager.Instance.efectos.clip = GameConfigurationManager.Instance.music_clips[4];
                    GameConfigurationManager.Instance.efectos.loop = false;
                    GameConfigurationManager.Instance.efectos.Play();
                    GameManager.Instance.isGameOver = true;
                    GameManager.Instance.waveCanvas.SetActive(false);
                    background.gameObject.SetActive(false);
                    GameManager.Instance.pauseCanvas.SetActive(false);
                    GameManager.Instance.victoryScreen.SetActive(true);
                    if (nivel_actual == GameConfigurationManager.Instance.unlockedLevel)
                    {
                        GameConfigurationManager.Instance.unlockedLevel++;
                        GameConfigurationManager.Instance.SaveLevel();
                    }
                }
                

                
                
            }
        }
    }
}
