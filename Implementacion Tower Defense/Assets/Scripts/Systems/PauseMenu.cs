using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool IsPaused = false;

    public GameObject PauseMenuUI; 
    public GameObject SettingsMenuUI;
    public GameObject PauseButton;

    public void Exit()
    {
        GameConfigurationManager.Instance.SaveLevel();
        SceneManager.LoadScene(0);
        
    }

    public void Settings()
    {
        SettingsMenuUI.SetActive(true);
    }

    public void GoBack()
    {
        SettingsMenuUI.SetActive(false);

        GameConfigurationManager.Instance.qualityLevel = QualitySettings.GetQualityLevel();
        GameConfigurationManager.Instance.SaveLevel();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (IsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

     public void Resume()
    {
        PauseMenuUI.SetActive(false);
        PauseButton.SetActive(true);
        Debug.Log(TowerManager.isMenuOpen);
        if (TowerManager.isMenuOpen == false)
        {
            Time.timeScale = 1f;
        }
        else
        {
            Time.timeScale = 0f;
        }
        
        IsPaused = false;
    }

    public void Pause()
    {
        Time.timeScale = 0f;
        PauseMenuUI.SetActive(true);
        PauseButton.SetActive(false);
        IsPaused = true;
    }

}
