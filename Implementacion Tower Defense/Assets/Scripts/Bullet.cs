using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Enemy target;
    private float dmg;
    public float bulletSpeed = 100f;
    private bool isBossKiller = false;
    private targetHit targetHit;
    public List<Enemy> enemyList;
    private MeshRenderer test;

    void Start()
    {
        test = GetComponent<MeshRenderer>();

    }

    public void SetBullet(Enemy target, float dmg, bool isBossKiller, targetHit targetType)
    {
        this.target = target;
        this.dmg = dmg;
        this.isBossKiller = isBossKiller;
        this.targetHit = targetType;
    }

    // Update is called once per frame
    void Update()
    {
        if(target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.transform.position.x, target.transform.position.y*1.5f, target.transform.position.z), bulletSpeed * Time.deltaTime);
            float distance = Vector3.Distance(transform.position, new Vector3(target.transform.position.x, target.transform.position.y * 1.5f, target.transform.position.z));
            if(distance <=2f && dmg == 0)
            {
                Destroy(gameObject);
            }
            if (distance < 0.1f)
            {
                if (isBossKiller)
                {
                    if (target.isBoss)
                    {
                        this.dmg = dmg*5;
                    }
                    else
                    {
                        this.dmg = dmg / 2;
                    }
                }
                else
                {
                    if(target.isBoss)
                    {
                        this.dmg = dmg/2;
                    }
                }
                if (targetHit == targetHit.Splash)
                {
                    //play bullet effect once
                    enemyList = Physics.OverlapSphere(gameObject.transform.position, 10).Where(currentEnemy => currentEnemy.GetComponent<Enemy>()).Select(currentEnemy => currentEnemy.GetComponent<Enemy>()).ToList();
                    for (int i = 0; i < enemyList.Count(); i++)
                    {
                        enemyList[i].TakeDamage(dmg);
                    }
                    Destroy(gameObject);
                }
                else if(targetHit == targetHit.SpecialSplash)
                {
                    StartCoroutine(SlowSplash(1 - (dmg / 100)));
                }
                else if (targetHit == targetHit.SpecialSingle) 
                {
                    StartCoroutine(SlowSingle(1- (dmg/100)));
                }
                else
                {
                    if (target.isDead == false)
                    {
                        target.TakeDamage(dmg);
                    }
                    Destroy(gameObject);
                }
                
            }
        }
        
    }
    public IEnumerator SlowSingle(float slow)
    {
        test.enabled = false;
        target.Slow(slow);
        target.myAnimatiorController.SetFloat("Slow", slow);
        yield return new WaitForSeconds(2);
        if(target.isDead == false)
        {
            target.SpeedUp();
            target.myAnimatiorController.SetFloat("Slow", 1);
        }
        
        
        Destroy(gameObject);
    }
    public IEnumerator SlowSplash(float slow)
    {
        test.enabled = false;
        enemyList = Physics.OverlapSphere(gameObject.transform.position, 10).Where(currentEnemy => currentEnemy.GetComponent<Enemy>()).Select(currentEnemy => currentEnemy.GetComponent<Enemy>()).ToList();
        for (int i = 0; i < enemyList.Count(); i++)
        {
            enemyList[i].Slow(slow);
            enemyList[i].myAnimatiorController.SetFloat("Slow", slow);
        }
        yield return new WaitForSeconds(2);
        for (int i = 0; i < enemyList.Count(); i++)
        {
            if (target.isDead == false)
            {
                enemyList[i].SpeedUp();
                enemyList[i].myAnimatiorController.SetFloat("Slow", 1);
            }
        }
        Destroy(gameObject);
    }
}
