using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public Animator myAnimatiorController;

    [Header("Stats")]
    public float maxHealth;
    public float maxSpeed;
    public int moneyDropped;
    public int damage;

    [Header("Special")]
    public bool isBoss;

    [Header("Movement")]
    public NavMeshAgent myNavMeshAgent;

    [Header("Status")]
    public bool isDead;
    public bool reachedEnd = false;
    public float currentHealth;
    public float currentSpeed;
    public Image fillLifeImage;
    public Transform canvasRoot;
    private Quaternion initLifeRotation;
    public AudioClip deathsound;

    public AudioSource noise;


    // Start is called before the first frame update
    private void Awake()
    {
        myNavMeshAgent = GetComponent<NavMeshAgent>();
        myAnimatiorController = GetComponentInChildren<Animator>();
        noise = gameObject.AddComponent<AudioSource>();
    }
    void Start()
    {
        canvasRoot = fillLifeImage.transform.parent.parent;
        initLifeRotation = canvasRoot.rotation;
        maxHealth = Mathf.RoundToInt(maxHealth * GameManager.Instance.levelCurve);
        currentHealth = maxHealth;
        currentSpeed = maxSpeed;
        myNavMeshAgent.speed = maxSpeed;
        myNavMeshAgent.destination = GameManager.Instance.endingPosition.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead == false)
        {
            canvasRoot.transform.rotation = initLifeRotation;
            
            if (!myNavMeshAgent.pathPending)
            {
                if (myNavMeshAgent.remainingDistance <= myNavMeshAgent.stoppingDistance)
                {
                    if (myNavMeshAgent.pathStatus == NavMeshPathStatus.PathComplete && myNavMeshAgent.remainingDistance >= 0 && TowerManager.checkingPath == false)
                    {
                        isDead = true;
                        GameManager.Instance.lives = GameManager.Instance.lives - gameObject.GetComponent<Enemy>().damage;
                        GameManager.Instance.UpdateLives();
                        GameManager.Instance.spawnManager.enemyList.Remove(this);
                        Destroy(this.gameObject);
                    }
                }
            }
        }

    }
    

    public void TakeDamage(float dmg)
    {
        var newHealth = currentHealth - dmg;
        if (newHealth <= 0)
        {
            OnDeath();
        }
        else
        {
            myAnimatiorController.SetTrigger("Damage");
            fillLifeImage.fillAmount = (currentHealth - dmg) / maxHealth;
            currentHealth = newHealth;
        }
    }



    public void Slow(float slow)
    {
        if(isDead == false)
        {
            myNavMeshAgent.speed = currentSpeed * slow;
        }
        
    }

    public void SpeedUp()
    {
        if (isDead == false)
        {
            myNavMeshAgent.speed = maxSpeed;
        }
    }

    public void OnDeath()
    {
        isDead = true;
        currentHealth = 0;
        fillLifeImage.fillAmount = 0;
        noise.clip = deathsound;
        noise.volume = GameConfigurationManager.Instance.effectVolume / 2;
        noise.Play();
        myAnimatiorController.SetTrigger("Death");
        this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
    
        this.gameObject.GetComponent<Collider>().enabled = false;
        this.gameObject.GetComponent<NavMeshAgent>().avoidancePriority = 2;
        this.gameObject.GetComponent<NavMeshAgent>().speed = 0;
        StartCoroutine(EnemyDespawn(3.25f));
        GameManager.Instance.money += this.moneyDropped;
        GameManager.Instance.UpdateMoney();
    }

    public IEnumerator EnemyDespawn(float delay)
    {
        yield return new WaitForSeconds(delay);
        for (int i = 0; i < GameManager.Instance.spawnManager.enemyList.Count; i++)
        {
            if (GameManager.Instance.spawnManager.enemyList[i] == this)
            {
                GameManager.Instance.spawnManager.enemyList.RemoveAt(i);
                Destroy(this.gameObject);
            }
        }

    }

}
