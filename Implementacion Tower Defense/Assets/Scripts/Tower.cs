using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public enum upgradeLevel { 
    nivel0, 
    nivel1, 
    nivel2
}

public enum targetType
{
    First,
    Last,
    Strongest,
    Random
}

public enum targetHit
{
    Single,
    Splash,
    SpecialSingle,
    SpecialSplash
}

public class Tower : MonoBehaviour
{
    [Header("Info")]
    public string towerName;
    public string towerDescription;


    [Header("Stats")]
    public float damage;
    public float attackSpeed;
    public float range;
    public int purchasePrice;
    public int upgradePrice;
    public int sellPrice;
    

    [Header("Special")]
    public bool isBossKiller;
    public targetHit targetHit;
    public bool isDual;

    [Header("Upgrades")]
    public upgradeLevel currentLevel;

    [Header("Enemies")]
    public Enemy currentTarget;
    public List<Enemy> currentTargets = new List<Enemy>();
    public targetType targetType;
    public float lookingPercent;

    public Transform rotationPart;
    public Transform shootPosition;
    public Transform shootPosition2;
    public GameObject shootEffect;
    public GameObject shootEffect2;
    public Tower towerUpgrade;
    public bool changedTarget;
    public Bullet bullet;


   
    // Start is called before the first frame update
    void Start()
    {
        shootEffect.SetActive(false);
        shootEffect2.SetActive(false);
        StartCoroutine(ShootTimer());
    }

    // Update is called once per frame
    void Update()
    {
        EnemyDetection();
        LookAtEnemy();
    }


    public void EnemyDetection()
    {

        //mirar alternativas
        currentTargets = Physics.OverlapSphere(gameObject.transform.position, (range+1)).Where(currentEnemy => currentEnemy.GetComponent<Enemy>()).Select(currentEnemy => currentEnemy.GetComponent<Enemy>()).ToList();
        if (currentTargets.Count() > 0)
        {
            if (currentTarget != null && currentTarget.isDead == true)
            {
                currentTarget = null;
                currentTargets.Remove(currentTarget);
                
            }
            else if (currentTarget == null || !currentTargets.Contains(currentTarget) || changedTarget == true)
            {
                changedTarget = false;
                if (targetType == targetType.First)
                {
                    var aux = currentTargets[0];
                    var temp = currentTargets[0].myNavMeshAgent.remainingDistance;
                    for (int i = 0; i< currentTargets.Count(); i++)
                    {
                        if (currentTargets[i].myNavMeshAgent.remainingDistance < temp)
                        {
                            temp = currentTargets[i].myNavMeshAgent.remainingDistance;
                            aux = currentTargets[i];
                        }
                        
                    }
                    currentTarget = aux;


                }
                else if (targetType == targetType.Last)
                {
                    var aux = currentTargets[0];

                    for (int i = 0; i < GameManager.Instance.spawnManager.enemyList.Count(); i++)
                    {
                        for (int j = 0; j < currentTargets.Count(); j++)
                        {
                            if (aux != null)
                            {
                                if (GameManager.Instance.spawnManager.enemyList[i] == currentTargets[j])
                                {
                                    aux = GameManager.Instance.spawnManager.enemyList[i].GetComponent<Enemy>();
                                    break;
                                }
                            }
                            else
                            {
                                aux = currentTargets[0];
                                break;
                            }
                        }
                    }
                    currentTarget = aux;
                }
                else if (targetType == targetType.Strongest)
                {
                    var aux =currentTargets[0];

                    for (int i = 0; i < currentTargets.Count(); i++)
                    {
                        if(aux!= null)
                        {
                            if (currentTargets[i].currentHealth > aux.currentHealth)
                            {
                                aux = currentTargets[i];
                            }
                        }
                        else
                        {
                            aux = currentTargets[0];
                        }
                        
                    }
                    
                    currentTarget = aux;
                }
                else if (targetType == targetType.Random)
                {
                    int index = Random.Range(0, currentTargets.Count());
                    currentTarget = currentTargets[index];
                }
            }
        }
        else
        {
            currentTarget = null;
        }
    }

    public void LookAtEnemy()
    {
        if (currentTarget)
        {
            lookingPercent = Mathf.MoveTowards(lookingPercent, 1f, Time.deltaTime);
            Vector3 relativePos = currentTarget.transform.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            rotationPart.rotation = Quaternion.Slerp(rotationPart.rotation, rotation, lookingPercent*0.1f);
            
        }else
        {
            lookingPercent = 0.25f;
        }
    }

    private IEnumerator ShootTimer()
    {
        
        while (true)
        {
            if (currentTarget && lookingPercent > 0.95f)
            {
                Shoot();
                if (QualitySettings.GetQualityLevel() != 0)
                {
                    shootEffect.SetActive(true);
                    if (isDual)
                    {
                        shootEffect2.SetActive(true);
                    }
                    var soundclip = shootEffect.GetComponentInChildren<AudioSource>();
                    if (soundclip != null)
                    {
                        soundclip.volume = GameConfigurationManager.Instance.effectVolume*2;
                        soundclip.PlayOneShot(soundclip.clip);
                    }
                    soundclip = null;
                    StartCoroutine(DeactivateShootEffect());
                }
                yield return new WaitForSeconds(1 / attackSpeed);
            }
            yield return null;
        }

    }

    private void Shoot()
    {


        if (isDual)
        {
            var shotBullet = Instantiate(bullet, shootPosition.position, shootPosition.rotation);
            var shotBullet2 = Instantiate(bullet, shootPosition2.position, shootPosition2.rotation);
            shotBullet.SetBullet(currentTarget, damage, isBossKiller,targetHit);
            shotBullet2.SetBullet(currentTarget, 0, isBossKiller, targetHit);



        }
        else
        {
            
            var shotBullet = Instantiate(bullet, shootPosition.position, shootPosition.rotation);
            shotBullet.SetBullet(currentTarget, damage, isBossKiller, targetHit);
        }
        
    }

    public IEnumerator DeactivateShootEffect()
    {
        yield return new WaitForSeconds(0.15f);
            shootEffect.SetActive(false);
            if (isDual)
            {
                shootEffect2.SetActive(false);
            }
    }

    private void OnDrawGizmos()
    {
        DebugExtension.DebugCircle(transform.position, Color.green, (range - 0.05f));
        DebugExtension.DebugCircle(transform.position, Color.green, (range - 0.1f));
        DebugExtension.DebugCircle(transform.position, Color.green, range);
        DebugExtension.DebugCircle(transform.position, Color.green, (range + 0.1f));
        DebugExtension.DebugCircle(transform.position, Color.green, (range + 0.05f));
    }
}
